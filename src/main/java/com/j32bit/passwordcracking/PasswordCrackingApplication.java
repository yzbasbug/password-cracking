package com.j32bit.passwordcracking;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.j32bit.passwordcracking.model.PasswordDigitData;
import com.j32bit.passwordcracking.service.PasswordCrackingService;
import com.j32bit.passwordcracking.service.PasswordCrackingServiceLocator;

import lombok.extern.log4j.Log4j2;

@SpringBootApplication
@Log4j2
public class PasswordCrackingApplication {

	private PasswordCrackingService passwordCrackingService;
	private static final int PASSWORD_LENGTH = 10;

	public static void main(String[] args) {
		SpringApplication.run(PasswordCrackingApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(PasswordCrackingServiceLocator passwordCrackingServiceLocator) {
		return args -> {
			long startTime = System.nanoTime();
			log.info("Let's crack the password!");
			log.info("We have a password with 10 digits");
			log.info("First digit represents the count of the zero in password");
			log.info("Second digit represents the count of the one in password");
			log.info("Third digit represents the count of the two in password");
			log.info("The same logic goes on until the last digit which represents the count of nine in the password.");
			PasswordDigitData[] initialPasswordArray = new PasswordDigitData[PASSWORD_LENGTH];
			// we need to initialise the initialPasswordArray
			for (int i = 0; i < PASSWORD_LENGTH; i++) {
				StringBuilder sb = new StringBuilder();
				sb.append("This digit represents the count of ");
				sb.append(i);
				sb.append(" numbers in the password");
				initialPasswordArray[i] = new PasswordDigitData(0, i, sb.toString());
			}
			if (passwordCrackingService == null) {
				passwordCrackingService = passwordCrackingServiceLocator.getPasswordCrackingService();
			}
			String crackedPassword = passwordCrackingService.crackPassword(initialPasswordArray);
			long elapsedTime = System.nanoTime() - startTime;
			log.debug("--------------------------------------------------------\n");
			log.debug("PASSWORD IS : {} , Elapsed Time in nanoseconds : {}\n", crackedPassword, elapsedTime);
			log.debug("--------------------------------------------------------\n");
		};
	}
}
