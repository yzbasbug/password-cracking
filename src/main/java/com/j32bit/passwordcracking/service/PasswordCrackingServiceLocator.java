package com.j32bit.passwordcracking.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class PasswordCrackingServiceLocator {
	private final Map<String, PasswordCrackingService> dynamicPasswordCrackingServiceMap;
	@Value("${passwordcracking.service.implementation.name}")
	private String implementationName;

	public PasswordCrackingServiceLocator(Map<String, PasswordCrackingService> dynamicPasswordCrackingServiceMap) {
		this.dynamicPasswordCrackingServiceMap = dynamicPasswordCrackingServiceMap;
	}

	public PasswordCrackingService getPasswordCrackingService() {
		log.trace("getPasswordCrackingService begins. implementationName: {}", implementationName);
		return this.dynamicPasswordCrackingServiceMap.get(implementationName);
	}
}
