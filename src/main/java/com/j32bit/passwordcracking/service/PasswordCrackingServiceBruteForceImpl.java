package com.j32bit.passwordcracking.service;

import org.springframework.stereotype.Service;

import com.j32bit.passwordcracking.model.PasswordDigitData;
import com.j32bit.passwordcracking.util.PasswordUtil;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service(value = "dummy")
public class PasswordCrackingServiceBruteForceImpl implements PasswordCrackingService {

	@Override
	public String crackPassword(PasswordDigitData[] passwordArray) {
		log.trace("crackPassword begins.passwordArray is {}", PasswordUtil.convertPasswordArray2String(passwordArray));
		if (PasswordUtil.checkPasswordArray(passwordArray)) {
			return PasswordUtil.convertPasswordArray2String(passwordArray);
		} else {
			passwordArray = PasswordUtil.increaseOne(passwordArray);
			return crackPassword(passwordArray);
		}
	}

}
