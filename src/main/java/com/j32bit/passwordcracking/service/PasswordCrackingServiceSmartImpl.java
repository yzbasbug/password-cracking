package com.j32bit.passwordcracking.service;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import com.j32bit.passwordcracking.model.PasswordDigitData;
import com.j32bit.passwordcracking.util.PasswordUtil;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service(value = "smart")
public class PasswordCrackingServiceSmartImpl implements PasswordCrackingService {

	private static final int WAITSEC = 5;

	@Override
	public String crackPassword(PasswordDigitData[] passwordArray) {
		log.trace("crackPassword begins.passwordArray is {}", PasswordUtil.convertPasswordArray2String(passwordArray));
		HashMap<Integer, Integer> digitCountMap = PasswordUtil.findCountOfEachNumber(passwordArray);
		for (int i = 0; i < passwordArray.length; i++) {
			PasswordDigitData passwordDigitData = passwordArray[i];
			Integer newNumber = digitCountMap.get(passwordDigitData.getRepresentedDigit());
			Integer existingNumber = passwordDigitData.getNumber();
			if (newNumber == null) {
				newNumber = 0;
			}
			passwordDigitData.setNumber(newNumber);

			// if existing number and new number is not same then we need to update counts
			// of this numbers.
			if (existingNumber != newNumber) {
				Integer newNumberCount = digitCountMap.get(newNumber);
				Integer existingNumberCount = digitCountMap.get(existingNumber);
				if (newNumberCount == null) {
					newNumberCount = 0;
				}
				if (existingNumberCount == null) {
					existingNumberCount = 0;
				}
				// Increase the count of newNumber
				newNumberCount++;
				// Decrease the count of oldNumber
				existingNumberCount--;
				if (existingNumberCount <= 0) {
					existingNumberCount = 0;
				}
				digitCountMap.put(newNumber, newNumberCount);
				digitCountMap.put(existingNumber, existingNumberCount);

			}
		}
		if (PasswordUtil.checkPasswordArray(passwordArray)) {
			return PasswordUtil.convertPasswordArray2String(passwordArray);
		} else {

			try {
				log.warn("Password is incorrect! We need to wait {} sec.", WAITSEC);
				Thread.sleep(WAITSEC * 1000);
			} catch (InterruptedException e) {
				log.error(e, e);
			}

			return crackPassword(passwordArray);
		}
	}

}
