package com.j32bit.passwordcracking.service;

import com.j32bit.passwordcracking.model.PasswordDigitData;

public interface PasswordCrackingService {

	/**
	 * Find the correct password according to given informations.
	 * 
	 * @param passwordArray
	 * @return cracked password as string
	 */
	public String crackPassword(PasswordDigitData[] passwordArray);
}
