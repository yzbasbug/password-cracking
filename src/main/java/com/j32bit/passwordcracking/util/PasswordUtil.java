package com.j32bit.passwordcracking.util;

import java.util.HashMap;

import com.j32bit.passwordcracking.model.PasswordDigitData;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class PasswordUtil {

	/**
	 * Convert the numbers as integer, increase one , add zero paddings and finally
	 * returns new passwordArray
	 * 
	 * @param passwordArray
	 * @return passwordArray
	 */
	public static PasswordDigitData[] increaseOne(PasswordDigitData[] passwordArray) {
		log.trace("increaseOne begins. Length of passwordArray is {}", passwordArray.length);
		String passwordArrayStr = convertPasswordArray2String(passwordArray);
		Integer passwordNumber = Integer.valueOf(passwordArrayStr);
		passwordNumber++;
		passwordArrayStr = String.valueOf(passwordNumber);
		while (passwordArrayStr.length() < 10) {
			passwordArrayStr = "0" + passwordArrayStr;
		}
		for (int i = 0; i < passwordArray.length; i++) {
			int number = Integer.valueOf(Character.toString(passwordArrayStr.charAt(i)));
			passwordArray[i].setNumber(number);
		}
		log.trace("increaseOne ends");
		return passwordArray;

	}

	/**
	 * Check the given password array is good or not good.
	 * 
	 * @param passwordArray
	 * @return status of validation as boolean
	 */
	public static boolean checkPasswordArray(PasswordDigitData[] passwordArray) {
		log.trace("checkPasswordArray begins. passwordArray : {}", convertPasswordArray2String(passwordArray));
		HashMap<Integer, Integer> digitCountMap = findCountOfEachNumber(passwordArray);
		for (int k = 0; k < 10; k++) {
			PasswordDigitData passwordDigitData = passwordArray[k];
			Integer newNumber = digitCountMap.get(passwordDigitData.getRepresentedDigit());
			// If founded is different than existing one then show must go on :)
			if (newNumber != null && newNumber != passwordDigitData.getNumber()) {
				log.trace("checkPasswordArray ends. Status: False");
				return false;
			}
		}
		log.trace("checkPasswordArray ends. Status: True");
		return true;
	}

	/**
	 * Found the counts of the numbers in the given array and return it as HashMap
	 * 
	 * @param passwordArray
	 * @return digitCountMap
	 */
	public static HashMap<Integer, Integer> findCountOfEachNumber(PasswordDigitData[] passwordArray) {
		log.trace("findCountOfEachNumber begins. Length of passwordArray is {}", passwordArray.length);
		HashMap<Integer, Integer> digitCountMap = new HashMap<>();
		for (int i = 0; i < passwordArray.length; i++) {
			int number = passwordArray[i].getNumber();
			Integer existingCount = digitCountMap.get(number);
			if (existingCount == null) {
				existingCount = 0;
			}
			// Should be digit
			if (existingCount < 9) {
				existingCount++;
			}

			digitCountMap.put(number, existingCount);
		}
		log.debug("digitCountMap: {}", digitCountMap);
		log.trace("findCountOfEachNumber ends");
		return digitCountMap;
	}

	/**
	 * Convert given password array to string using number attribute of
	 * PasswordDigitData
	 * 
	 * @param passwordArray
	 * @return passwordArrayStr
	 */
	public static String convertPasswordArray2String(PasswordDigitData[] passwordArray) {
		log.trace("convertPasswordArray2String begins. Length of passwordArray is {}", passwordArray.length);
		StringBuilder sb = new StringBuilder();
		for (int i = 0; passwordArray != null && i < passwordArray.length; i++) {
			sb.append(passwordArray[i].getNumber());
		}
		String passwordArrayStr = sb.toString();
		log.trace("convertPasswordArray2String ends. passwordArray is {}", passwordArrayStr);
		return passwordArrayStr;
	}
}
